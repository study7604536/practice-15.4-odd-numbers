﻿// Practice_15.4.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>

void NumbersPrint(const int Limit)
{
    // Если число четное то первое число должно быть 0 в противном случае 1
    int Start = Limit % 2;
    std::cout << "If number is " << Limit << " numbers are \n";
    for (int i = 0 + Start; i <= Limit; i = i + 2)
    {
        std::cout << i << " ";
    }
}

int main()
{
    NumbersPrint(15);
}

